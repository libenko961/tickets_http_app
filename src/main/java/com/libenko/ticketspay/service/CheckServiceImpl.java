package com.libenko.ticketspay.service;

import com.libenko.ticketspay.entity.Ticket;
import com.libenko.ticketspay.repository.TicketRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Random;

@Slf4j
@Service
public class CheckServiceImpl implements CheckService {
    private static final String PROCESSED_STATUS = "PROCESSED";
    private static final String ERROR_STATUS = "ERROR";
    private static final String POSTED_STATUS = "POSTED";

    private final TicketRepository repository;
    private final Random random;

    @Autowired
    public CheckServiceImpl(TicketRepository repository, Random random) {
        this.repository = repository;
        this.random = random;
    }

    @Override
    public String updateStatus(Long id) {
        Ticket ticket = repository.findById(id).orElseThrow(RuntimeException::new);
        ticket.setStatus(randomStatus(random.nextInt(3), ticket.getStatus()));
        log.info("Status after randomStatus {}", ticket.getStatus());
        repository.save(ticket);
        return ticket.getStatus();
    }

    private boolean checkStatus(String status) {
        return status == null || status.equals(PROCESSED_STATUS);
    }

    private String randomStatus(int random, String status) {
        log.info("Method randomStatus accepted : {}", status);
        if (checkStatus(status)) {
            switch (random) {
                case 0:
                    return PROCESSED_STATUS;
                case 1:
                    return ERROR_STATUS;
                case 2:
                    return POSTED_STATUS;
                default:
                    throw new IllegalArgumentException();
            }
        }
        return status;
    }
}
