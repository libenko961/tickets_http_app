package com.libenko.ticketspay.service;

import com.libenko.ticketspay.entity.Ticket;
import com.libenko.ticketspay.repository.TicketRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Slf4j
@Service
public class ReceptionServiceImpl implements ReceptionService {

    private final TicketRepository repository;

    @Autowired
    public ReceptionServiceImpl(TicketRepository repository) {
        this.repository = repository;
    }

    @Override
    public Long save(int number, LocalDateTime departureTime) {
        log.info("Method save in process");

        return repository.save(new Ticket(number, departureTime)).getId();
    }
}
