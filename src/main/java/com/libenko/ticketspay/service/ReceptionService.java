package com.libenko.ticketspay.service;

import java.time.LocalDateTime;

public interface ReceptionService {
    Long save(int number, LocalDateTime departureTime);
}
