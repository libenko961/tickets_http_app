package com.libenko.ticketspay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TicketsPayApplication {
    public static void main(String[] args) {
        SpringApplication.run(TicketsPayApplication.class, args);
    }
}
