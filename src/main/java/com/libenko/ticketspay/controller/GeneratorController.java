package com.libenko.ticketspay.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.LocalDateTime;
import java.util.Random;

@RestController
@RequestMapping("/generated")
@Slf4j
@EnableScheduling
public class GeneratorController {
    private static final String RECEIPT_URL = "http://localhost:8080/reception/receipt";
    private final RestTemplate restTemplate;
    private final Random random;

    @Autowired
    public GeneratorController(RestTemplate restTemplate, Random random) {
        this.restTemplate = restTemplate;
        this.random = random;
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @Scheduled(fixedRate = 60000)
    public void postGeneratedData() {
        UriComponentsBuilder uri = UriComponentsBuilder
                .fromHttpUrl(RECEIPT_URL)
                .queryParam("Number", random.nextInt(100))
                .queryParam("DateTime", LocalDateTime.now().toString());
        log.info("Generated new uri : {}", uri.toUriString());
        restTemplate.postForEntity(uri.toUriString(), Integer.class, String.class);
    }
}

