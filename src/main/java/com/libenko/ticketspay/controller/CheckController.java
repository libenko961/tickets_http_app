package com.libenko.ticketspay.controller;

import com.libenko.ticketspay.service.CheckService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@RestController
@Slf4j
@Validated
@RequestMapping("/check")
public class CheckController {
    private final CheckService service;

    @Autowired
    public CheckController(CheckService service) {
        this.service = service;
    }

    @PutMapping(path = "/status", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getStatus(@RequestParam(value = "id") @NotNull @Positive Long id) {
        log.info("method getStatus accepted id: {}", id);

        return new ResponseEntity<>(service.updateStatus(id), HttpStatus.OK);
    }
}
