package com.libenko.ticketspay.controller;

import com.libenko.ticketspay.service.ReceptionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.time.LocalDateTime;

@RestController
@Slf4j
@Validated
@RequestMapping("/reception")
public class ReceptionController {
    private static final String CHECK_URL = "http://localhost:8080/check/status";
    private final ReceptionService service;
    private final RestTemplate restTemplate;

    @Autowired
    public ReceptionController(ReceptionService service, RestTemplate restTemplate) {
        this.service = service;
        this.restTemplate = restTemplate;
    }

    @PostMapping(path = "/receipt", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Long> getId(@RequestParam(value = "Number") @NotNull @Positive int number,
                                      @RequestParam(value = "DateTime")
                                      @NotNull
                                      @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
                                              LocalDateTime dateTime) {
        log.info("Method getId accepted: {}, {}", number, dateTime);
        Long id = service.save(number, dateTime);
        UriComponentsBuilder uri = UriComponentsBuilder
                .fromHttpUrl(CHECK_URL)
                .queryParam("id", id);
        log.info("uri for transmission {}", uri.toUriString());
        restTemplate.put(uri.toUriString(), Long.class);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }
}
